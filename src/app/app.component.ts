import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import formJson from './form.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'pre-test-json-form';
  formJson = formJson;
  submitForm = new FormGroup({});

  constructor() {
    console.log(formJson);
    for(var i = 0; i < formJson.form.length; i++) {
      this.submitForm.setControl(formJson.form[i].id, new FormControl(formJson.form[i].value));
    }
    console.log(this.submitForm.controls);
  }

  ngOnInit() {}

  submitted() {
    console.log(this.submitForm.controls);
  }

}
